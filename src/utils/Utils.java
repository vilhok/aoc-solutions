package utils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Utils {

    private Utils() {
    }

    public static final int[][] ROT_X_MATRIX = new int[][] { //
	    { 1, 0, 0 }, //
	    { 0, 0, -1 }, //
	    { 0, 1, 0 } //
    };

    public long[] parseLongs(String strings) {
	return parseLongs(strings, false);
    }

    public long[] parseLongs(String strings, String sep) {
	return parseLongs(strings, sep, false);
    }

    public static long[] parseLongs(String strings, boolean skipFirst) {
	return parseLongs(strings, "\\s+", skipFirst);
    }

    public static long[] parseLongs(String strings, String sep, boolean skipFirst) {
	System.out.println(strings);
	String[] x = strings.split(sep);
	int idx = skipFirst ? 1 : 0;

	long[] ints = new long[x.length - idx];
	for (int i = 0; i < ints.length; i++) {
	    ints[i] = Long.parseLong(x[i + idx]);
	}
	return ints;
    }

    public static List<Point> fourNeighbors() {
	return List.of(new Point(-1, 0), new Point(1, 0), new Point(0, 1), new Point(0, -1));
    }

    public static List<Point> eightNeighbors() {
	ArrayList<Point> p = new ArrayList<>();
	for (int x = -1; x <= 1; x++) {
	    for (int y = -1; y <= 1; y++) {
		if (!(x == 0 && y == 0)) {
		    p.add(new Point(x, y));
		}
	    }
	}
	return p;
    }

    public static List<Point> nineNeighbors() {
	ArrayList<Point> p = new ArrayList<>();
	for (int y = -1; y <= 1; y++) {
	    for (int x = -1; x <= 1; x++) {

		p.add(new Point(x, y));

	    }
	}
	return p;
    }
}
