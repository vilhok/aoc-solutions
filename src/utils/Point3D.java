package utils;

import java.util.Comparator;

public record Point3D(long x, long y, long z) implements Comparable<Point3D> {

	public String toString() {
		return String.format("[%s,%s,%s]", x, y, z);
	}

	public Point3D add(Point3D diff) {
		return new Point3D(this.x + diff.x, this.y + diff.y, this.z + diff.z);
	}

	public long manhattan(Point3D o) {
		return Math.abs(this.x - o.x) + Math.abs(this.y - o.y) + Math.abs(this.z - o.z);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (x ^ (x >>> 32));
		result = prime * result + (int) (y ^ (y >>> 32));
		result = prime * result + (int) (z ^ (z >>> 32));
		return result;
	}

	@Override
	public int compareTo(Point3D o) {
		Comparator<Point3D> c = Comparator.comparing(Point3D::x).thenComparing(Point3D::y).thenComparing(Point3D::z);
		return c.compare(this, o);
	}

}
