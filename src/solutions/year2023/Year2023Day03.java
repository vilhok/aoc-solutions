package solutions.year2023;

import java.util.Arrays;
import java.util.List;
import java.util.function.Function;

import com.github.aoclib.api.InputParser;
import com.github.aoclib.solver.PuzzleSolver;

import utils.Point;
import utils.Utils;

/**
 * <p>
 * AUTOGENERATED BY DayGenerator.java
 * </p>
 *
 * <p>
 * https://gitlab.com/vilhok/aoc-lib
 * </p>
 *
 * <p>
 * Edits in this file will not be overwritten.
 * </p>
 *
 */
public class Year2023Day03 extends PuzzleSolver {

    boolean isNumber(char[][] map, int row, int col) {
	return map[row][col] <= '9' && map[row][col] >= '0';
    }

    int extractNumber(char[][] map, int row, int col, boolean[][] visited) {
	StringBuilder number = new StringBuilder();
	int orig = col;
	while (isNumber(map, row, col)) {
	    number.append(map[row][col]);
	    visited[row][col] = true;
	    col++;
	    if (col >= map[0].length)
		break;
	}
	col = orig - 1;

	if (col >= 0)
	    while (isNumber(map, row, col)) {
		number.insert(0, map[row][col]);
		visited[row][col] = true;
		col--;
		if (col < 0) {
		    break;
		}
	    }
	return Integer.parseInt(number.toString());
    }

    boolean hasSymbolAdjacent(char[][] map, int row, int col, boolean[][] visited) {

	if (row < 0 || col < 0 || row >= map.length || col >= map.length) {
	    return false;
	}
	if (map[row][col] == '.') {
	    return false;
	}
	if (!isNumber(map, row, col)) {
	    return true;
	}

	List<Point> neighbors = Utils.eightNeighbors();
	for (Point n : neighbors) {
	    int checkRow = row + n.y();
	    int checkCol = col + n.x();
	    if (checkRow < 0 || checkCol < 0 || checkRow >= map.length || checkCol >= map.length) {
		continue;
	    }
	    if (!visited[checkRow][checkCol]) {
		if (isNumber(map, row, col)) {
		    visited[row][col] = true;
		}
		boolean hasSymbol = hasSymbolAdjacent(map, checkRow, checkCol, visited);
		if (hasSymbol) {
		    return true;
		}
	    }

	}
	return false;
    }

    @Override
    public Object firstPart(InputParser input) {
	char[][] chars = input.charMatrix();

	// for avoiding parsing same numbers twice
	boolean[][] numberParserVisited = new boolean[chars.length][chars[0].length];

	// for checking symbols, but don't mark the symbols itself visited.
	boolean[][] symbolCheckerVisited = new boolean[chars.length][chars[0].length];

	int sum = 0;
	for (int row = 0; row < chars.length; row++) {
	    for (int col = 0; col < chars[row].length; col++) {
		boolean isNumber = isNumber(chars, row, col);
		if (isNumber) {
		    if (!numberParserVisited[row][col]) {
			int num = extractNumber(chars, row, col, numberParserVisited);

			if (hasSymbolAdjacent(chars, row, col, symbolCheckerVisited)) {
			    sum += num;
			}
		    }
		}
	    }
	}
	return sum;
    }

    @Override
    public Object secondPart(InputParser input) {
	char[][] chars = input.charMatrix();
	boolean[][] numberParserVisited = new boolean[chars.length][chars[0].length];
	int sum = 0;
	for (int row = 0; row < chars.length; row++) {
	    for (int col = 0; col < chars[row].length; col++) {
		if (chars[row][col] == '*') {
		    int product = 1;
		    int numcount = 0;
		    for (Point p : Utils.eightNeighbors()) {
			int dr = row + p.y();
			int dc = col + p.x();
			if (isNumber(chars, dr, dc) && !numberParserVisited[dr][dc]) {
			    int num = extractNumber(chars, dr, dc, numberParserVisited);
			    product *= num;
			    numcount++;
			}
		    }
		    if (numcount == 2)
			sum += product;
		}

	    }
	}
	return sum;
    }

    public void printVisited(boolean[][] symbolExtractorVisited) {
	for (boolean[] v : symbolExtractorVisited) {
	    System.out.println(Arrays.toString(v));
	}
    }

    /*
     * Optional: add tests for each part in the following methods
     *
     * These methods have blank implementations in superclass as well and can be
     * deleted if you don't want to include tests.
     *
     * Add test as follows:
     *
     * new Test("sampleinput", expectedSolution);
     *
     * Collect the tests from the task web page.
     */

    @Override
    protected void insertTestsPart1(List<Test> tests) {
	String s = """
		467..114..
		...*......
		..35..633.
		......#...
		617*......
		.....+.58.
		..592.....
		......755.
		...$.*....
		.664.598..""";
	tests.add(new Test(s, 4361));
    }

    @Override
    protected void insertTestsPart2(List<Test> tests) {
	String s = """
		467..114..
		...*......
		..35..633.
		......#...
		617*......
		.....+.58.
		..592.....
		......755.
		...$.*....
		.664.598..""";
	tests.add(new Test(s, 467835));
    }
}
