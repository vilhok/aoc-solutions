package solutions.year2023;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import com.github.aoclib.api.InputParser;
import com.github.aoclib.solver.PuzzleSolver;

/**
 * <p>
 * AUTOGENERATED BY DayGenerator.java
 * </p>
 *
 * <p>
 * https://gitlab.com/vilhok/aoc-lib
 * </p>
 *
 * <p>
 * Edits in this file will not be overwritten.
 * </p>
 *
 */
public class Year2023Day05 extends PuzzleSolver {

    class Mapper {
	String source;
	String target;
	Mapper next;
	ArrayList<RowMapper> rows;

	public String toString() {
	    if (next == null) {
		return target + "";
	    }
	    return source + "->" + next.toString();
	}

	class RowMapper {
	    long inputStart;
	    long conversionStart;
	    long range;

	    public RowMapper(long inputStart, long conversionStart, long range) {
		this.inputStart = inputStart;
		this.conversionStart = conversionStart;
		this.range = range - 1;

	    }

	    long map(long value) {
		if (value >= inputStart && value <= inputStart + range) {
		    long delta = value - inputStart;
		    return conversionStart + delta;
		}
		return value;
	    }

	    public String toString() {
		return target + " " + source + " " + (range + 1);
	    }

	}


	public long map(long v) {
	    long nextStep = v;
	    for (RowMapper cr : rows) {
		long attempt = cr.map(v);
		if (attempt != v) {
		    nextStep = attempt;
		    break;
		}
	    }
	    return next == null ? nextStep : next.map(nextStep);
	}

	public Mapper(String source, String target) {
	    this.source = source;
	    this.target = target;
	    this.rows = new ArrayList<>();

	}

	void addNext(Mapper cm) {
	    this.next = cm;
	}

	public void addRange(long input, long output, long count) {
	    rows.add(new RowMapper(input, output, count));
	}

    }

    Mapper constructMap(List<String> list, boolean reversed) {
	String names = list.remove(0);
	String[] srcDest = names.split("[- ]");
	Mapper cm = new Mapper(srcDest[reversed ? 2 : 0], srcDest[reversed ? 0 : 2]);
	for (String s : list) {
	    String[] values = s.split(" ");
	    cm.addRange(Long.parseLong(values[reversed ? 0 : 1]), Long.parseLong(values[reversed ? 1 : 0]),
		    Long.parseLong(values[2]));

	}
	return cm;
    }

    void connectAll(HashMap<String, Mapper> chains) {
	for (Mapper c : chains.values()) {
	    Mapper other = chains.get(c.target);
	    if (other != null) {
		c.addNext(other);
	    }
	}
    }

    boolean validSeed(long[] seedInts, long seed) {
	for (int i = 0; i < seedInts.length; i += 2) {
	    if (seed >= seedInts[i] && seed < seedInts[i] + seedInts[i + 1]) {

		return true;
	    }
	}
	return false;
    }

    @Override
    public Object firstPart(InputParser input) {
	List<List<String>> groups = input.getGroups();
	String seedStr = input.getLines().get(0).substring("seeds: ".length());
	String[] seeds = seedStr.split(" ");

	HashMap<String, Mapper> mappers = new HashMap<>();
	for (List<String> list : groups) {
	    Mapper cm = constructMap(list, false);

	    mappers.put(cm.source, cm);

	}
	connectAll(mappers);
	Mapper startingPoint = mappers.get("seed");

	long low = Long.MAX_VALUE;
	for (String s : seeds) {
	    long l = Long.parseLong(s);
	    long conversion = startingPoint.map(l);
	    if (conversion < low) {
		low = conversion;
	    }
	}
	return low;
    }

    @Override
    public Object secondPart(InputParser input) {
	List<List<String>> groups = input.getGroups();
	String seedStr = input.getLines().get(0).substring("seeds: ".length());
	String[] seeds = seedStr.split(" ");
	long[] seedInts = Arrays.stream(seeds).mapToLong(i -> Long.parseLong(i)).toArray();
	HashMap<String, Mapper> mappers = new HashMap<>();
	for (List<String> list : groups) {
	    Mapper cm = constructMap(list, true);

	    mappers.put(cm.source, cm);

	}
	connectAll(mappers);
	Mapper startingPoint = mappers.get("location");
	long nextLocation = 0;
	while (true) {
	    long seed = startingPoint.map(nextLocation);
	    if (validSeed(seedInts, seed)) {
		break;
	    }
	    nextLocation++;
	}

	return nextLocation;
    }

    /*
     * Optional: add tests for each part in the following methods
     *
     * These methods have blank implementations in superclass as well and can be
     * deleted if you don't want to include tests.
     *
     * Add test as follows:
     *
     * new Test("sampleinput", expectedSolution);
     *
     * Collect the tests from the task web page.
     */

    @Override
    protected void insertTestsPart1(List<Test> tests) {
	String t = """
		seeds: 79 14 55 13

		seed-to-soil map:
		50 98 2
		52 50 48

		soil-to-fertilizer map:
		0 15 37
		37 52 2
		39 0 15

		fertilizer-to-water map:
		49 53 8
		0 11 42
		42 0 7
		57 7 4

		water-to-light map:
		88 18 7
		18 25 70

		light-to-temperature map:
		45 77 23
		81 45 19
		68 64 13

		temperature-to-humidity map:
		0 69 1
		1 0 69

		humidity-to-location map:
		60 56 37
		56 93 4""";
	tests.add(new Test(t, 35));
    }

    @Override
    protected void insertTestsPart2(List<Test> tests) {
	String t = """
		seeds: 79 14 55 13

		seed-to-soil map:
		50 98 2
		52 50 48

		soil-to-fertilizer map:
		0 15 37
		37 52 2
		39 0 15

		fertilizer-to-water map:
		49 53 8
		0 11 42
		42 0 7
		57 7 4

		water-to-light map:
		88 18 7
		18 25 70

		light-to-temperature map:
		45 77 23
		81 45 19
		68 64 13

		temperature-to-humidity map:
		0 69 1
		1 0 69

		humidity-to-location map:
		60 56 37
		56 93 4""";
	tests.add(new Test(t, 46));
    }
}
