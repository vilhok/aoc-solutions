package solutions.year2023;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

import com.github.aoclib.api.InputParser;
import com.github.aoclib.solver.PuzzleSolver;

/**
 * <p>
 * AUTOGENERATED BY DayGenerator.java
 * </p>
 *
 * <p>
 * https://gitlab.com/vilhok/aoc-lib
 * </p>
 *
 * <p>
 * Edits in this file will not be overwritten.
 * </p>
 *
 */
public class Year2023Day10 extends PuzzleSolver {

    Map<Character, Character> replaced = Map.of('J', '┘', 'L', '└', '7', '┐', 'F', '┌');
    Map<Character, Character> regular = Map.of('J', 'J', 'L', 'L', '7', '7', 'F', 'F');

    char[] toLeft = { '-', 'J', '7' };
    char[] toRight = { '-', 'L', 'F' };
    char[] toDown = { '|', '7', 'F' };
    char[] toUp = { '|', 'J', 'L' };

    char[] fromDown = { '|', '7', 'F' };
    char[] fromUp = { '|', 'J', 'L' };
    char[] fromLeft = { '-', 'J', '7' };
    char[] fromRight = { '-', 'L', 'F' };

    public boolean canAccess(char[] chars, char c) {
	for (char k : chars) {
	    if (c == k) {
		return true;
	    }
	}
	return false;
    }

    int maxdepth = 0;

    public void recurseRoutes(char[][] chars, int row, int col, int[][] distances, int depth) {
	int currentValue = distances[row][col] == -1 ? 0 : distances[row][col];
	int c = depth + 1;
	if (c > maxdepth) {
	    maxdepth = c;
	}
	if (row > 0 && canAccess(toUp, chars[row][col]) && canAccess(fromDown, chars[row - 1][col])) {
	    if (distances[row - 1][col] == 0 || distances[row - 1][col] > currentValue + 1) {
		distances[row - 1][col] = currentValue + 1;
		recurseRoutes(chars, row - 1, col, distances, depth + 1);
	    }
	}

	if (row < chars.length - 1 && canAccess(toDown, chars[row][col]) && canAccess(fromUp, chars[row + 1][col])) {
	    if (distances[row + 1][col] == 0 || distances[row + 1][col] > currentValue + 1) {
		distances[row + 1][col] = currentValue + 1;
		recurseRoutes(chars, row + 1, col, distances, depth + 1);
	    }
	}

	if (col > 0 && canAccess(toLeft, chars[row][col]) && canAccess(fromRight, chars[row][col - 1])) {
	    if (distances[row][col - 1] == 0 || distances[row][col - 1] > currentValue + 1) {
		distances[row][col - 1] = currentValue + 1;
		recurseRoutes(chars, row, col - 1, distances, depth + 1);
	    }
	}
	if (col < chars[0].length - 1 && canAccess(toRight, chars[row][col])
		&& canAccess(fromLeft, chars[row][col + 1])) {
	    if (distances[row][col + 1] == 0 || distances[row][col + 1] > currentValue + 1) {
		distances[row][col + 1] = currentValue + 1;
		recurseRoutes(chars, row, col + 1, distances, depth + 1);
	    }
	}

    }

    @Override
    public Object firstPart(InputParser input) {
	char[][] chars = input.charMatrix();
	Map<Character, Character> map = regular;
	int col = 0, row = 0;
	for (int i = 0; i < chars.length; i++) {
	    if (col > 0 && canAccess(toDown, chars[row][col])) {

	    }
	    for (int j = 0; j < chars.length; j++) {
		if (map.containsKey(chars[i][j]))
		    chars[i][j] = map.get(chars[i][j]);
		if (chars[i][j] == 'S') {
		    col = j;
		    row = i;
		}
	    }
	}
	int[][] distances = new int[chars.length][chars[0].length];
	distances[row][col] = -1;

	inferS(chars, row, col);
//	System.out.println(row + " " + col);
	recurseRoutes(chars, row, col, distances, 0);
	System.out.println(maxdepth);
//	printdistances(distances); 

	return Arrays.stream(distances).flatMap(x -> Arrays.stream(x).boxed()).mapToInt(i -> i).max().getAsInt();
    }

    private void inferS(char[][] chars, int row, int col) {
	for (char c : new char[] { '-', '|', 'L', 'F', '7', 'J' }) {
	    chars[row][col] = c;
	    if (accessCount(chars, row, col) == 2) {
		chars[row][col] = c;
		return;
	    }
	}
	throw new RuntimeException("NO connection available");
    }

    public int accessCount(char[][] chars, int row, int col) {
	int count = 0;
	if (row > 0 && canAccess(toUp, chars[row][col]) && canAccess(fromDown, chars[row - 1][col])) {
	    count++;
	}

	if (row < chars.length - 1 && canAccess(toDown, chars[row][col]) && canAccess(fromUp, chars[row + 1][col])) {
	    count++;
	}

	if (col > 0 && canAccess(toLeft, chars[row][col]) && canAccess(fromRight, chars[row][col - 1])) {
	    count++;
	}
	if (col < chars[0].length - 1 && canAccess(toRight, chars[row][col])
		&& canAccess(fromLeft, chars[row][col + 1])) {
	    count++;
	}
	return count;
    }

    private void printdistances(int[][] distances) {
	for (int[] onerow : distances) {
	    for (int c : onerow) {
		if (c == -1)
		    System.out.print("S");
		else
		    System.out.print(c);

	    }
	    System.out.println();
	}
    }

    @Override
    public Object secondPart(InputParser input) {
	return NOT_SOLVED;
    }

    /*
     * Optional: add tests for each part in the following methods
     *
     * These methods have blank implementations in superclass as well and can be
     * deleted if you don't want to include tests.
     *
     * Add test as follows:
     *
     * new Test("sampleinput", expectedSolution);
     *
     * Collect the tests from the task web page.
     */

    @Override
    protected void insertTestsPart1(List<Test> tests) {
	String t1 = """
		.....
		.S-7.
		.|.|.
		.L-J.
		.....
		""";
	String t2 = """
		..F7.
		.FJ|.
		SJ.L7
		|F--J
		LJ...
		""";
	tests.add(new Test(t1, 4));
	tests.add(new Test(t2, 8));
    }

    @Override
    protected void insertTestsPart2(List<Test> tests) {
	tests.add(new Test(p21, 4));
	tests.add(new Test(p22, 4));
	tests.add(new Test(p23, 8));
    }

    String p21 = """
    	...........
    	.S-------7.
    	.|F-----7|.
    	.||.....||.
    	.||.....||.
    	.|L-7.F-J|.
    	.|..|.|..|.
    	.L--J.L--J.
    	...........
    	""";
    String p22 = """
    	..........
    	.S------7.
    	.|F----7|.
    	.||OOOO||.
    	.||OOOO||.
    	.|L-7F-J|.
    	.|II||II|.
    	.L--JL--J.
    	..........""";

    String p23 = """
    	.F----7F7F7F7F-7....
    	.|F--7||||||||FJ....
    	.||.FJ||||||||L7....
    	FJL7L7LJLJ||LJ.L-7..
    	L--J.L7...LJS7F-7L7.
    	....F-J..F7FJ|L7L7L7
    	....L7.F7||L7|.L7L7|
    	.....|FJLJ|FJ|F7|.LJ
    	....FJL-7.||.||||...
    	....L---J.LJ.LJLJ...
    	""";
}
