package solutions.year2019;

import com.github.aoclib.api.InputParser;
import com.github.aoclib.solver.PuzzleSolver;

public class Year2019Day08 extends PuzzleSolver {
    @Override
    public Object firstPart(InputParser input) {
	String s = input.string();

	int size = 25 * 6;

	int result = 0;
	int ones = Integer.MAX_VALUE;
	while (s.length() > 0) {
	    String layer = s.substring(0, size);
	    s = s.substring(size);
	    int cnt = countchar('0', layer);
	    if (cnt < ones) {
		ones = cnt;
		result = countchar('1', layer) * countchar('2', layer);
	    }
	}
	return result;
    }

    public int countchar(char c, String s) {
	int cnt = 0;
	for (char v : s.toCharArray()) {
	    if (v == c) {
		cnt++;
	    }
	}
	return cnt;
    }

    @Override
    public Object secondPart(InputParser input) {
	String s = input.string();

	int size = 25 * 6;

	char[][][] layers = new char[6][25][s.length() / size];

	int row = 0, col = 0, layer = 0;

	for (char c : s.toCharArray()) {
	    layers[row][col][layer] = c;
	    col++;
	    if (col >= 25) {
		col = 0;
		row++;
	    }
	    if (row >= 6) {
		row = 0;
		col = 0;
		layer++;
	    }

	}
	char[][] finalImage = new char[6][25];

	System.out.println(layers.length);
	System.out.println(layers[0].length);
	System.out.println(layers[0][0].length);
	for (int r = 0; r < 6; r++) {
	    for (int c = 0; c < 25; c++) {
		int topLayer = 0;
		while (topLayer < layers[0][0].length - 1 && layers[r][c][topLayer] == '2') {
		    topLayer++;
		}
		finalImage[r][c] = layers[r][c][topLayer];
	    }
	}
	for (int r = 0; r < 6; r++) {
	    for (int c = 0; c < 25; c++) {
		System.out.print(finalImage[r][c] == '1' ? "█" : " ");
	    }
	    System.out.println();

	}
	return NOT_SOLVED;
    }
}