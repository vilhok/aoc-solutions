package solutions.year2020;

import java.util.HashSet;

import com.github.aoclib.api.InputParser;
import com.github.aoclib.solver.PuzzleSolver;

public class Year2020Day01 extends PuzzleSolver {

	@Override
	public Object firstPart(InputParser input) {

		return firstPartAlt(input);

//		int[] values = input.asSingleIntArray();
//		int a = 0, b = 0;
//
//		for (int i : values) {
//			for (int j : values) {
//				if (i + j == 2020) {
//					return i * j;
//				}
//			}
//		}
//		return a * b;

	}

	// O(n)
	public Object firstPartAlt(InputParser input) {

		int[] values = input.asSingleIntArray();
		int a = 0, b = 0;
		HashSet<Integer> subtractions = new HashSet<>();
		int target = 18;
		for (int i : values) {
			if (subtractions.contains(i)) {
				a = i;
				b = target - i;
				return a * b;
			} else {
				subtractions.add(target - i);
			}
		}
		return NOT_SOLVED;
	}

	@Override
	public Object secondPart(InputParser input) {
		int[] values = input.asSingleIntArray();

		if (1 == 1)
			return NOT_SOLVED;
		for (int i : values) {
			for (int j : values) {
				for (int k : values) {
					if (i + j + k == 2020) {

						return i * j * k;
					}
				}
			}
		}

		return NOT_SOLVED;
	}
}