package solutions.year2016;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.github.aoclib.api.InputParser;
import com.github.aoclib.solver.PuzzleSolver;

public class Year2016Day09 extends PuzzleSolver {

    String regex = "[A-Z]*";
    String numRegex = "\\(([0-9]*)x([0-9]*)\\)";
    String beginningRegex = "([A-Z]*)\\(";

    @Override
    public Object firstPart(InputParser input) {
	StringBuilder expanded = new StringBuilder();
	String s = input.string();

	Matcher m = Pattern.compile(numRegex).matcher(s);

	while (m.find()) {

	    expanded.append(s.subSequence(0, s.indexOf("(")));
	    s = s.substring(s.indexOf("("));

	    int g = m.group(0).length();
	    int first = Integer.parseInt(m.group(1));
	    int second = Integer.parseInt(m.group(2));
	    s = s.substring(s.indexOf(m.group(0)));
	    s = s.substring(g);
	    String cut = s.substring(0, first);
	    expanded.append(cut.repeat(second));
	    s = s.substring(first);
	    m = Pattern.compile(numRegex).matcher(s);
	}
	expanded.append(s);
	return expanded.length();
    }

    @Override
    public Object secondPart(InputParser input) {
	long totalSize = 0;
	StringBuilder s = new StringBuilder(input.string());

	System.out.println(s);
	Matcher m = Pattern.compile(numRegex).matcher(s);
	int roundsDone = 0;
	while (m.find()) {
	    Matcher begin = Pattern.compile(beginningRegex).matcher(s);
	    begin.find();
	    int decompressedFromBeginnin = begin.group(1).length();
	    totalSize += decompressedFromBeginnin;
	    int instructionLength = m.group(0).length();

//	    System.out.println(m.group(0));
	    int first = Integer.parseInt(m.group(1));
	    int second = Integer.parseInt(m.group(2));
//	    System.out.println("beforedelete;" + s);
	    s.delete(0, decompressedFromBeginnin + instructionLength);
//	    System.out.println("afterdelete:" + s);
	    String cut = s.substring(0, first);
	    s.delete(0, first);
	    String repeatedPart = cut.repeat(second);
	    s.insert(0, repeatedPart);
	    m = Pattern.compile(numRegex).matcher(s);
	    roundsDone++;
	    if (roundsDone % 10000 == 0) {
		System.out.println(roundsDone + " " + s.length() + " " + totalSize);
	    }
	}
	totalSize += s.length();
	System.out.println(totalSize);
	return totalSize;
    }

    public static long solve(String s, boolean partTwo) {
	long cnt = 0;
	char[] s_arr = s.toCharArray();
	for (int i = 0; i < s_arr.length; i++) {
	    if (s_arr[i] == ' ')
		continue;
	    else if (s_arr[i] == '(') {
		int end = s.indexOf(')', i);
		int howMuch = Integer.valueOf(s.substring(i + 1, end).split("x")[0]);
		int times = Integer.valueOf(s.substring(i + 1, end).split("x")[1]);
		String repeat = s.substring(end + 1, end + 1 + howMuch);
		cnt += times * ((partTwo) ? solve(repeat, partTwo) : repeat.length());
		i = end + howMuch;
	    } else
		cnt++;
	}
	return cnt;

    }

    class Tree {

	ArrayList<Node> nodes;

	public Tree(String input) {
//	nodes = new ArrayList<>();
//	Matcher begin = Pattern.compile(beginningRegex).matcher(input);
//	begin.find();
//	int decompressedFromBeginnin = begin.group(1).length();
//	if (decompressedFromBeginnin > 0) {
//	    nodes.add(new Node(decompressedFromBeginnin, 1));
//	} else {
//
//	}

	}

	public long result() {
	    long result = 0;
	    for (Node child : nodes) {
		result += child.result();
	    }
	    return result;
	}

	class Node {
	    ArrayList<Node> nodes;
	    int multiplier;
	    int value;

	    public Node(int value, int multiplier) {
		this.multiplier = multiplier;
		this.value = value;
	    }

	    public Node(int value) {
		this(value, 1);
	    }

	    public long result() {
		long result = 0;
		for (Node child : nodes) {
		    result += child.result();
		}
		return result * multiplier;
	    }
	}

    }

    @Override
    public void insertTestsPart1(List<Test> tests) {
	tests.add(new Test("X(8x2)(3x3)ABCY", 18));
	tests.add(new Test("(6x1)(1x3)A", 6));
	tests.add(new Test("A(2x2)BCD(2x2)EFG", 11));
	tests.add(new Test("A(1x5)BC", 7));
	tests.add(new Test("(3x3)XYZ", 9));
    }

    @Override
    public void insertTestsPart2(List<Test> tests) {
	tests.add(new Test("X(8x2)(3x3)ABCY", "XABCABCABCABCABCABCY".length()));
	tests.add(new Test("(27x12)(20x12)(13x14)(7x10)(1x12)A", 241920));
	tests.add(new Test("(25x3)(3x3)ABC(2x3)XY(5x2)PQRSTX(18x9)(3x2)TWO(5x7)SEVEN", 445));
    }
}