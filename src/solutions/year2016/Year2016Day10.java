package solutions.year2016;

import com.github.aoclib.api.InputParser;
import com.github.aoclib.solver.PuzzleSolver;

public class Year2016Day10 extends PuzzleSolver {

	@Override
	public Object firstPart(InputParser input) {
		return NOT_SOLVED;
	}

	@Override
	public Object secondPart(InputParser input) {
		return NOT_SOLVED;
	}
}