package solutions.year2021;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.github.aoclib.api.InputParser;
import com.github.aoclib.solver.PuzzleSolver;

import utils.Point3D;

/**
 * <p>
 * AUTOGENERATED BY DayGenerator.java
 * </p>
 *
 * <p>
 * https://github.com/vilhok/aoc-lib
 * </p>
 *
 * <p>
 * Edits in this file will not be overwritten.
 * </p>
 *
 */
public class Year2021Day22 extends PuzzleSolver {

	String regex = "(.*) x=(.*)\\.\\.(.*),y=(.*)\\.\\.(.*),z=(.*)\\.\\.(.*)";

	public void turnCubesOn(Set<Point3D> on, Point3D lower, Point3D upper) {
		for (long z = lower.z(); z <= upper.z(); z++) {
			for (long y = lower.y(); y <= upper.y(); y++) {
				for (long x = lower.x(); x <= upper.x(); x++) {
					on.add(new Point3D(x, y, z));
				}
			}
		}
	}

	public void turnCubesOff(Set<Point3D> on, Point3D lower, Point3D upper) {
		for (long z = lower.z(); z <= upper.z(); z++) {
			for (long y = lower.y(); y <= upper.y(); y++) {
				for (long x = lower.x(); x <= upper.x(); x++) {
					on.remove(new Point3D(x, y, z));
				}
			}
		}
	}

	static class Cube implements Comparable<Cube> {

		boolean on;
		final Point3D min;
		final Point3D max;

		public Cube(boolean on, Point3D min, Point3D max) {
			this.on = on;
			this.min = min;
			this.max = max;
		}

		public Cube(Point3D min, Point3D max) {
			this(false, min, max);
		}

		public long size() {
			long s = (Math.abs(max.x() - min.x()) + 1) * (Math.abs(max.y() - min.y()) + 1)
					* (Math.abs(max.z() - min.z()) + 1);
			;
			if (s == 0) {
				throw new RuntimeException("Size calculation failed");

			}
			return s;
		}

		public void turnOn() {
			this.on = true;
		}

		public void turnOff() {
			this.on = false;
		}

		@Override
		public String toString() {
			return "Cube[" + min + "," + max + "]";
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + ((max == null) ? 0 : max.hashCode());
			result = prime * result + ((min == null) ? 0 : min.hashCode());
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			Cube other = (Cube) obj;
			if (max == null) {
				if (other.max != null)
					return false;
			} else if (!max.equals(other.max))
				return false;
			if (min == null) {
				if (other.min != null)
					return false;
			} else if (!min.equals(other.min))
				return false;
			return true;
		}

		@Override
		public int compareTo(Cube o) {
			int result = this.min.compareTo(o.min);
			if (result != 0) {
				return result;
			}

			return this.max.compareTo(o.max);
		}

	}

	record CubeInstruction(boolean on, Point3D min, Point3D max) {

		public boolean contains(Cube other) {
			return other.min.x() >= this.min.x()//
					&& other.min.y() >= this.min.y() //
					&& other.min.z() >= this.min.z() //
					&& other.max.x() <= this.max.x() //
					&& other.max.y() <= this.max.y()//
					&& other.max.z() <= this.max.z();
		}

		public boolean contains(Point3D p) {
			return contains(new Cube(false, p, p));
		}

		/*
		 * public List<Cube> split(Cube other) {
		 * 
		 * if (other.contains(this.)
		 * 
		 * if (!this.overlaps(other)) { return List.of(this, other); } // overlap, there
		 * will be an 8-way split, for both. int xintersection;
		 * 
		 * if (this.max.x() >= other.min.x()) { // int xsplit = x
		 * 
		 * } return List.of(); }
		 */
		public boolean insideX(CubeInstruction other) {
			return other.min.x() <= this.min.x() && other.max.x() >= this.max.x();
		}

		public boolean insideY(CubeInstruction other) {
			return other.min.y() <= this.min.y() && other.max.y() >= this.max.y();
		}

		public boolean insideZ(CubeInstruction other) {
			return other.min.z() <= this.min.z() && other.max.z() >= this.max.z();
		}

		public void test(CubeInstruction other) {
			if (other.insideX(this)) {
				System.out.println("x:" + this + " " + other);
			}
			if (other.insideY(this)) {
				System.out.println("y:" + this + " " + other);
			}
			if (other.insideZ(this)) {
				System.out.println("z:" + this + " " + other);
			}
		}

		/**
		 * 
		 * @param other
		 * @return
		 */
		public boolean overlaps(CubeInstruction other) {
			return other.max.x() >= this.min.x() //
					&& other.max.y() >= this.min.y()//
					&& other.max.z() >= this.min.z() //
					&& other.min.x() <= this.max.x() //
					&& other.min.y() <= this.max.y()//
					&& other.min.z() <= this.max.z();
		}

		public long size() {
			return (Math.abs(max.x() - min.x()) + 1) * (Math.abs(max.y() - min.y()) + 1)
					* (Math.abs(max.z() - min.z()) + 1);
		}

		public long size_old_wrong() {
			return Math.abs(max.x() - min.x()) * Math.abs(max.y() - min.y()) * Math.abs(max.z() - min.z());
		}

		public CubeInstruction translate(long x, long y, long z) {
			Point3D low = new Point3D(min.x() + x, min.y() + y, min.z() + z);
			Point3D high = new Point3D(max.x() + x, max.y() + y, max.z() + z);

			return new CubeInstruction(on, low, high);
		}
	}

	@Override
	public Object firstPart(InputParser input) {
		List<String> lines = input.getLines();

		Set<Point3D> on = new HashSet<>();

		ArrayList<CubeInstruction> cubes = new ArrayList<>();
		for (String s : lines) {
			Matcher m = Pattern.compile(regex).matcher(s);
			m.matches();

			String state = m.group(1);
			int minx = Integer.parseInt(m.group(2));
			int maxx = Integer.parseInt(m.group(3));
			int miny = Integer.parseInt(m.group(4));
			int maxy = Integer.parseInt(m.group(5));
			int minz = Integer.parseInt(m.group(6));
			int maxz = Integer.parseInt(m.group(7));

			Point3D min = new Point3D(minx, miny, minz);
			Point3D max = new Point3D(maxx, maxy, maxz);
			boolean turnOn = state.equals("on");
			CubeInstruction c = new CubeInstruction(turnOn, min, max);
			cubes.add(c);

			if (min.x() >= -50 && max.x() <= 50) {
				switch (state) {
					case "on" -> turnCubesOn(on, min, max);
					case "off" -> turnCubesOff(on, min, max);
				}
			}
//			System.out.println(state);
		}

		ArrayList<CubeInstruction> onNeverOverlap = new ArrayList<>();

		ArrayList<CubeInstruction> singlecubes = new ArrayList<>();
		// filter cubes
		/*
		 * find such cubes that are always on, without any overlaps find such cubes that
		 * are always off -> no effect
		 * 
		 * find such cubes that turn off at the end. Find out all such cubes that are
		 * completely contained by those
		 * 
		 * find such cubes that are turned on, and completely contained by other that is
		 * on, later.
		 */

		return on.size();

	}

	public void assrt(long x1, long y1, long z1, long x2, long y2, long z2, int count) {

		ArrayList<Cube> x = getCubes(x1, y1, z1, x2, y2, z2);
		if (x.size() != count) {
			System.out.println(x.size() + " " + count);
			for (Cube c : x) {
				System.out.println(c);
			}
			System.exit(0);
		}

	}

	@Override
	public Object secondPart(InputParser input) {
		List<String> lines = input.getLines();

//		assrt(0, 0, 0, 0, 0, 0, 1);
//		assrt(0, 0, 0, 0, 0, 1, 2);
//		assrt(0, 0, 0, 0, 1, 0, 2);
//		assrt(0, 0, 0, 1, 0, 0, 2);
//		assrt(0, 0, 0, 1, 0, 1, 4);
//		assrt(0, 0, 0, 0, 1, 1, 4);
////		assrt(0, 0, 0, 1, 0, 0, 2);
//		assrt(0, 0, 0, 1, 1, 0, 4);
//		assrt(0, 0, 0, 1, 1, 1, 9);
//		assrt(0, 0, 0, 1, 1, 2, 13);
//		assrt(0, 0, 0, 1, 2, 1, 13);
//		assrt(0, 0, 0, 10, 10, 10, 21);
		System.out.println("All tests done");
//		System.exit(0);

		ArrayList<CubeInstruction> cubes = new ArrayList<>();
		long minxt = Integer.MAX_VALUE;
		long minyt = Integer.MAX_VALUE;
		long minzt = Integer.MAX_VALUE;

		HashSet<Long> xSet = new HashSet<>();
		HashSet<Long> ySet = new HashSet<>();
		HashSet<Long> zSet = new HashSet<>();

		for (String s : lines) {
			Matcher m = Pattern.compile(regex).matcher(s);
			m.matches();

			String state = m.group(1);
			long minx = Integer.parseInt(m.group(2));
			long maxx = Integer.parseInt(m.group(3));
			long miny = Integer.parseInt(m.group(4));
			long maxy = Integer.parseInt(m.group(5));
			long minz = Integer.parseInt(m.group(6));
			long maxz = Integer.parseInt(m.group(7));

			Point3D min = new Point3D(minx, miny, minz);
			Point3D max = new Point3D(maxx, maxy, maxz);
			boolean turnOn = state.equals("on");
			CubeInstruction c = new CubeInstruction(turnOn, min, max);
			cubes.add(c);

			if (miny < minyt) {
				minyt = miny;
			}
			if (minx < minxt) {
				minxt = minx;
			}
			if (minz < minzt) {
				minzt = minz;
			}

			xSet.add(minx);
			xSet.add(maxx);

			ySet.add(miny);
			ySet.add(maxy);

			zSet.add(minz);
			zSet.add(maxz);

		}

		ArrayList<Long> xcoords = new ArrayList<>(xSet);
		ArrayList<Long> ycoords = new ArrayList<>(ySet);
		ArrayList<Long> zcoords = new ArrayList<>(zSet);

		Collections.sort(xcoords);
		Collections.sort(ycoords);
		Collections.sort(zcoords);

		HashSet<Cube> allCubes = new HashSet<>();
		int cnt = 0;
		long totalcubes = 0;
		ArrayList<CubeInstruction> added = new ArrayList<>();
		for (CubeInstruction testcube : cubes) {

			ArrayList<Long> xx = getWithinBounds(testcube.min.x(), testcube.max.x(), xcoords);
			ArrayList<Long> yy = getWithinBounds(testcube.min.y(), testcube.max.y(), ycoords);
			ArrayList<Long> zz = getWithinBounds(testcube.min.z(), testcube.max.z(), zcoords);
			HashSet<Cube> subs = explode(testcube, xx, yy, zz);
			totalcubes += subs.size();
			allCubes.addAll(subs);

			added.add(testcube);

			System.out.println("Explode-check for " + testcube);
			long ssum = subs.stream().filter(c -> testcube.contains(c)).mapToLong(Cube::size).sum();
			System.out.println("\tReturned sum vs. actual sum " + ssum + " " + testcube.size());
			if (testcube.size() != ssum) {
				System.out.println(ssum + " " + testcube.size());
				subs.stream().sorted().forEach(System.out::println);
				System.out.println("fail");
				System.exit(-1);
			}
			for (CubeInstruction t : added) {
				System.out.println("  Contains-check for set: " + t);
				long sum = allCubes.stream().filter(c -> t.contains(c)).mapToLong(Cube::size).sum();
				if (sum != t.size()) {

					System.exit(-1);
				}
			}

			cnt++;
			System.out.println(cnt + " " + cubes.size() + "processed. total cubes: " + allCubes.size());
			System.out.println("Unfiltered count: " + totalcubes);

		}

//		System.out.println(xcoords);
//		System.out.println(ycoords);
//		System.out.println(zcoords);newCubes
//
//		ArrayList<Cube> newCubes = new ArrayList<>();
//		System.out.println(xcoords.size() + " " + ycoords.size() + " " + zcoords.size());
//		for (int x = 0; x < xcoords.size() - 1; x++) {
//			for (int y = 0; y < ycoords.size() - 1; y++) {
//				for (int z = 0; z < zcoords.size() - 1; z++) {
//					long maxx = xcoords.get(x + 1);
//					long maxy = ycoords.get(y + 1);
//					long maxz = zcoords.get(z + 1);
//					Point3D low = new Point3D(xcoords.get(x), ycoords.get(y), zcoords.get(z));
//					Point3D high = new Point3D(maxx, maxy, maxz);
//					Cube candidate = new Cube(false, low, high);
//					boolean added = false;
//					for (CubeInstruction i : cubes) {
//						if (i.contains(candidate)) {
//							newCubes.add(candidate);
//							added = true;
//							break;
//						}
//					}
////					if(!added)
////					System.out.println(candidate);
//
//				}
//			}
//			System.out.println(newCubes.size() + " " + x);
//
//		}
////		System.out.println("Cubes:" + newCubes.size());

		for (CubeInstruction c : cubes) {
			long crnt = 0;
			System.out.println(c);
			for (Cube subcube : allCubes) {
//				System.out.println(c + " " + subcube);
				if (c.contains(subcube)) {
					crnt += subcube.size();
					if (c.on) {
						subcube.turnOn();
//						System.out.println("ON: " + subcube + " IN " + c);
					} else {
						subcube.turnOff();
//						System.out.println("OFF: " + subcube + " IN " + c);
					}
				} else {
//					System.out.println("Not contains:"+c+" " +subcube);
				}
			}
			System.out.println("Original:" + c.size() + " subcubes" + crnt);
		}
		// filter cubes
		/*
		 * find such cubes that are always on, without any overlaps find such cubes that
		 * are always off -> no effect
		 * 
		 * find such cubes that turn off at the end. Find out all such cubes that are
		 * completely contained by those
		 * 
		 * find such cubes that are turned on, and completely contained by other that is
		 * on, later.
		 */
//		Collections.reverse(cubes);
//
//		nextcube:
//
//		for (int i = 0; i < cubes.size(); i++) {
//			Cube outer = cubes.get(i);
//
//			for (int j = 0; j < cubes.size(); j++) {
//				Cube other = cubes.get(j);
//				if (other == outer)
//					continue;
//				if (outer.overlaps(other)) {
////					System.out.println(outer + " " + other);
//					// continue nextcube;
//					outer.test(other);
//				}
//				if (outer.contains(other)) {
//					System.out.println("yes");
//				}
//
//			}
//
//		}
//		ONLY_TEST = true;
//		System.out.println(cubes.size());
//
//		for (Cube c : onNeverOverlap) {
//			cubes.remove(c);
//		}
////		System.out.println(cubes.size());
//		long onlyOnSum = onNeverOverlap.stream().mapToLong(Cube::size).sum();
////		System.out.println(onlyOnSum);
//
//		ArrayList<Cube> another = new ArrayList<>();
//		for (Cube c : cubes) {
//			another.add(c);
////			System.out.println(c + " " + c.size());
////			System.out.println(another.stream().filter(Cube::on).mapToLong(Cube::size).sum());
//		}
//		
//		
		long s = allCubes.stream().filter(c -> c.on).mapToLong(Cube::size).sum();
		System.out.println("Count:" + s);
		return s;

	}

	/*
	 * Optional: add tests for each part in the following methods
	 *
	 * These methods have blank implementations in superclass as well and can be
	 * deleted if you don't want to include tests.
	 *
	 * Add test as follows:
	 *
	 * new Test("sampleinput", expectedSolution);
	 *
	 * Collect the tests from the task web page.
	 */

	private int count(ArrayList<Long> c, Long v) {
		int cnt = 0;
		for (Long l : c) {
			if (l.equals(v)) {
				cnt++;
			}
		}
		return cnt;
	}

	private ArrayList<Long> getWithinBounds(long low, long high, ArrayList<Long> values) {
		ArrayList<Long> retVal = new ArrayList<>();

		for (Long l : values) {
			if (l >= low) {
				if (l <= high) {
					retVal.add(l);
				} else {
					break;
				}
			}
		}
		return retVal;
	}

	private HashSet<Cube> explode(CubeInstruction ci, ArrayList<Long> xcoords, ArrayList<Long> ycoords,
			ArrayList<Long> zcoords) {
		HashSet<Cube> subcubes = new HashSet<>();
		System.out.println("Cutting instruction " + ci);
		System.out.println(xcoords + "\n" + ycoords + "\n" + zcoords);
		for (int x = 0; x < xcoords.size(); x++) {
			for (int y = 0; y < ycoords.size(); y++) {
				for (int z = 0; z < zcoords.size(); z++) {
//					System.out.println("subcube: " + x + " " + y + " " + z);
					long lowx = xcoords.get(x);
					long lowy = ycoords.get(y);
					long lowz = zcoords.get(z);
					long highx = (x + 1) == xcoords.size() ? xcoords.get(x) : (xcoords.get(x + 1) - 1);
					long highy = (y + 1) == ycoords.size() ? ycoords.get(y) : (ycoords.get(y + 1) - 1);
					long highz = (z + 1) == zcoords.size() ? zcoords.get(z) : (zcoords.get(z + 1) - 1);
					System.out.println(List.of(lowx, lowy, lowz, highx, highy, highz));
					subcubes.addAll(getCubes(lowx, lowy, lowz, highx, highy, highz));

				}
			}
		}
//		for (Cube k : subcubes) {
//			System.out.println(k);
//		}
		return subcubes;

	}

	private ArrayList<Cube> getCubes(long x1, long y1, long z1, long x2, long y2, long z2) {
		ArrayList<Cube> cubes = new ArrayList<>();

		// corners of this sub area
		for (long x : new HashSet<Long>(List.of(x1, x2))) {
			for (long y : new HashSet<Long>(List.of(y1, y2))) {
				for (long z : new HashSet<Long>(List.of(z1, z2))) {
//					System.out.println("Added");
					cubes.add(new Cube(new Point3D(x, y, z), new Point3D(x, y, z)));
				}
			}
		}
//		System.out.println("corners");
//		cubes.stream().sorted().forEach(System.out::println);
		// edges..
		if (x1 + 1 != x2 && x1 != x2) {
//			System.out.println("X");
			cubes.add(new Cube(new Point3D(x1 + 1, y1, z1), new Point3D(x2 - 1, y1, z1)));
			cubes.add(new Cube(new Point3D(x1 + 1, y2, z1), new Point3D(x2 - 1, y2, z1)));
			cubes.add(new Cube(new Point3D(x1 + 1, y1, z2), new Point3D(x2 - 1, y1, z2)));
			cubes.add(new Cube(new Point3D(x1 + 1, y2, z2), new Point3D(x2 - 1, y2, z2)));

		}
		if (y1 + 1 != y2 && y1 != y2) {
//			System.out.println("Y");
			cubes.add(new Cube(new Point3D(x1, y1 + 1, z1), new Point3D(x1, y2 - 1, z1)));
			cubes.add(new Cube(new Point3D(x2, y1 + 1, z1), new Point3D(x2, y2 - 1, z1)));
			cubes.add(new Cube(new Point3D(x1, y1 + 1, z2), new Point3D(x1, y2 - 1, z2)));
			cubes.add(new Cube(new Point3D(x2, y1 + 1, z2), new Point3D(x2, y2 - 1, z2)));
		}

		if (z1 + 1 != z2 && z1 != z2) {
//			System.out.println("Z");
			cubes.add(new Cube(new Point3D(x1, y1, z1 + 1), new Point3D(x1, y1, z2 - 1)));
			cubes.add(new Cube(new Point3D(x2, y1, z1 + 1), new Point3D(x2, y1, z2 - 1)));
			cubes.add(new Cube(new Point3D(x1, y2, z1 + 1), new Point3D(x1, y2, z2 - 1)));
			cubes.add(new Cube(new Point3D(x2, y2, z1 + 1), new Point3D(x2, y2, z2 - 1)));
		}

		// center

		long centerminx = getmin(x1, x2);
		long centermaxx = getmax(x1, x2);

		long centerminy = getmin(y1, y2);
		long centermaxy = getmax(y1, y2);
		
		long centerminz = getmin(z1, z2);
		long centermaxz = getmax(z1, z2);
		
//		System.out.println("added center:");
		Cube center = new Cube(new Point3D(centerminx,centerminy,centerminz), new Point3D(centermaxx,centermaxy,centermaxz));
		cubes.add(center);

		System.out.println("slicing finished:");
		cubes.stream().sorted().forEach(System.out::println);
		return cubes;
	}

	private long getmin(long low, long high) {
		return (low == high || low + 1 == high) ? low : low + 1;
	}

	private long getmax(long low, long high) {
		return (low == high || low + 1 == high) ? low : high - 1;
	}

	private void fix(ArrayList<Long> xcoords) {
		if (1 == 1)
			return;
		ArrayList<Long> tmp = new ArrayList<>();
		for (long l : xcoords) {
			int count = count(xcoords, l);
			if (count > 1) {
				tmp.add(l + 1);
			}
		}
		xcoords.addAll(tmp);

	}

	@Override
	protected void insertTestsPart1(List<Test> tests) {
		tests.add(new Test(t1data, 474140));
		tests.add(new Test(t1dataUpdated, 590784));
	}

	@Override
	protected void insertTestsPart2(List<Test> tests) {
		tests.add(new Test(tinyTest2, 39+27*2));
//		tests.add(new Test(superTinyTest, 26));
//		tests.add(new Test(t1dataUpdated, 590784));
//		tests.add(new Test(drawnTest, 0));
//		tests.add(new Test(t1dataUpdatedT2, 590784));
//		tests.add(new Test(t2data, 2758514936282235L));
	}

	// 3x3 cube centered in 0.0 fails when off is -1,-1,
	String superTinyTest = """
			on x=-1..1,y=-1..1,z=-1..1
			off x=0..0,y=0..0,z=-1..-1
			""";

	String drawnTest = """
			on x=-4..2,y=-4..-2,z=0..0
			off x=-2..3,y=-5..2,z=0..0
			on x=1..6,y=-3..-1,z=0..0
			""";

	String tinyTest = """
			on x=10..12,y=10..12,z=10..12
			on x=11..13,y=11..13,z=11..13
			off x=9..11,y=9..11,z=9..11
			on x=10..10,y=10..10,z=10..10
			on x=-10..-8,y=-10..-8,z=-10..-8
			on x=-1..1,y=-1..1,z=-1..1
			on x=-20..20,y=-20..20,z=-20..20
			""";
	
	String tinyTest2 = """
			on x=-1..1,y=-1..1,z=-1..1
			on x=-5..5,y=-5..5,z=-5..5
			""";

	String t1data = """
			on x=-5..47,y=-31..22,z=-19..33
			on x=-44..5,y=-27..21,z=-14..35
			on x=-49..-1,y=-11..42,z=-10..38
			on x=-20..34,y=-40..6,z=-44..1
			off x=26..39,y=40..50,z=-2..11
			on x=-41..5,y=-41..6,z=-36..8
			off x=-43..-33,y=-45..-28,z=7..25
			on x=-33..15,y=-32..19,z=-34..11
			off x=35..47,y=-46..-34,z=-11..5
			on x=-14..36,y=-6..44,z=-16..29
			on x=-54112..-39298,y=-85059..-49293,z=-27449..7877
			on x=967..23432,y=45373..81175,z=27513..53682""";

	String t1dataUpdated = """
			on x=-20..26,y=-36..17,z=-47..7
			on x=-20..33,y=-21..23,z=-26..28
			on x=-22..28,y=-29..23,z=-38..16
			on x=-46..7,y=-6..46,z=-50..-1
			on x=-49..1,y=-3..46,z=-24..28
			on x=2..47,y=-22..22,z=-23..27
			on x=-27..23,y=-28..26,z=-21..29
			on x=-39..5,y=-6..47,z=-3..44
			on x=-30..21,y=-8..43,z=-13..34
			on x=-22..26,y=-27..20,z=-29..19
			off x=-48..-32,y=26..41,z=-47..-37
			on x=-12..35,y=6..50,z=-50..-2
			off x=-48..-32,y=-32..-16,z=-15..-5
			on x=-18..26,y=-33..15,z=-7..46
			off x=-40..-22,y=-38..-28,z=23..41
			on x=-16..35,y=-41..10,z=-47..6
			off x=-32..-23,y=11..30,z=-14..3
			on x=-49..-5,y=-3..45,z=-29..18
			off x=18..30,y=-20..-8,z=-3..13
			on x=-41..9,y=-7..43,z=-33..15
			on x=-54112..-39298,y=-85059..-49293,z=-27449..7877
			on x=967..23432,y=45373..81175,z=27513..53682""";

	String t1dataUpdatedT2 = """
			on x=-20..26,y=-36..17,z=-47..7
			on x=-20..33,y=-21..23,z=-26..28
			on x=-22..28,y=-29..23,z=-38..16
			on x=-46..7,y=-6..46,z=-50..-1
			on x=-49..1,y=-3..46,z=-24..28
			on x=2..47,y=-22..22,z=-23..27
			on x=-27..23,y=-28..26,z=-21..29
			on x=-39..5,y=-6..47,z=-3..44
			on x=-30..21,y=-8..43,z=-13..34
			on x=-22..26,y=-27..20,z=-29..19
			off x=-48..-32,y=26..41,z=-47..-37
			on x=-12..35,y=6..50,z=-50..-2
			off x=-48..-32,y=-32..-16,z=-15..-5
			on x=-18..26,y=-33..15,z=-7..46
			off x=-40..-22,y=-38..-28,z=23..41
			on x=-16..35,y=-41..10,z=-47..6
			off x=-32..-23,y=11..30,z=-14..3
			on x=-49..-5,y=-3..45,z=-29..18
			off x=18..30,y=-20..-8,z=-3..13
			on x=-41..9,y=-7..43,z=-33..15""";

	String t1dataFor2 = """
			on x=-5..47,y=-31..22,z=-19..33
			on x=-44..5,y=-27..21,z=-14..35
			on x=-49..-1,y=-11..42,z=-10..38
			on x=-20..34,y=-40..6,z=-44..1
			off x=26..39,y=40..50,z=-2..11
			on x=-41..5,y=-41..6,z=-36..8
			off x=-43..-33,y=-45..-28,z=7..25
			on x=-33..15,y=-32..19,z=-34..11
			off x=35..47,y=-46..-34,z=-11..5
			on x=-14..36,y=-6..44,z=-16..29""";

	String t2data = """
			on x=-5..47,y=-31..22,z=-19..33
			on x=-44..5,y=-27..21,z=-14..35
			on x=-49..-1,y=-11..42,z=-10..38
			on x=-20..34,y=-40..6,z=-44..1
			off x=26..39,y=40..50,z=-2..11
			on x=-41..5,y=-41..6,z=-36..8
			off x=-43..-33,y=-45..-28,z=7..25
			on x=-33..15,y=-32..19,z=-34..11
			off x=35..47,y=-46..-34,z=-11..5
			on x=-14..36,y=-6..44,z=-16..29
			on x=-57795..-6158,y=29564..72030,z=20435..90618
			on x=36731..105352,y=-21140..28532,z=16094..90401
			on x=30999..107136,y=-53464..15513,z=8553..71215
			on x=13528..83982,y=-99403..-27377,z=-24141..23996
			on x=-72682..-12347,y=18159..111354,z=7391..80950
			on x=-1060..80757,y=-65301..-20884,z=-103788..-16709
			on x=-83015..-9461,y=-72160..-8347,z=-81239..-26856
			on x=-52752..22273,y=-49450..9096,z=54442..119054
			on x=-29982..40483,y=-108474..-28371,z=-24328..38471
			on x=-4958..62750,y=40422..118853,z=-7672..65583
			on x=55694..108686,y=-43367..46958,z=-26781..48729
			on x=-98497..-18186,y=-63569..3412,z=1232..88485
			on x=-726..56291,y=-62629..13224,z=18033..85226
			on x=-110886..-34664,y=-81338..-8658,z=8914..63723
			on x=-55829..24974,y=-16897..54165,z=-121762..-28058
			on x=-65152..-11147,y=22489..91432,z=-58782..1780
			on x=-120100..-32970,y=-46592..27473,z=-11695..61039
			on x=-18631..37533,y=-124565..-50804,z=-35667..28308
			on x=-57817..18248,y=49321..117703,z=5745..55881
			on x=14781..98692,y=-1341..70827,z=15753..70151
			on x=-34419..55919,y=-19626..40991,z=39015..114138
			on x=-60785..11593,y=-56135..2999,z=-95368..-26915
			on x=-32178..58085,y=17647..101866,z=-91405..-8878
			on x=-53655..12091,y=50097..105568,z=-75335..-4862
			on x=-111166..-40997,y=-71714..2688,z=5609..50954
			on x=-16602..70118,y=-98693..-44401,z=5197..76897
			on x=16383..101554,y=4615..83635,z=-44907..18747
			off x=-95822..-15171,y=-19987..48940,z=10804..104439
			on x=-89813..-14614,y=16069..88491,z=-3297..45228
			on x=41075..99376,y=-20427..49978,z=-52012..13762
			on x=-21330..50085,y=-17944..62733,z=-112280..-30197
			on x=-16478..35915,y=36008..118594,z=-7885..47086
			off x=-98156..-27851,y=-49952..43171,z=-99005..-8456
			off x=2032..69770,y=-71013..4824,z=7471..94418
			on x=43670..120875,y=-42068..12382,z=-24787..38892
			off x=37514..111226,y=-45862..25743,z=-16714..54663
			off x=25699..97951,y=-30668..59918,z=-15349..69697
			off x=-44271..17935,y=-9516..60759,z=49131..112598
			on x=-61695..-5813,y=40978..94975,z=8655..80240
			off x=-101086..-9439,y=-7088..67543,z=33935..83858
			off x=18020..114017,y=-48931..32606,z=21474..89843
			off x=-77139..10506,y=-89994..-18797,z=-80..59318
			off x=8476..79288,y=-75520..11602,z=-96624..-24783
			on x=-47488..-1262,y=24338..100707,z=16292..72967
			off x=-84341..13987,y=2429..92914,z=-90671..-1318
			off x=-37810..49457,y=-71013..-7894,z=-105357..-13188
			off x=-27365..46395,y=31009..98017,z=15428..76570
			off x=-70369..-16548,y=22648..78696,z=-1892..86821
			on x=-53470..21291,y=-120233..-33476,z=-44150..38147
			off x=-93533..-4276,y=-16170..68771,z=-104985..-24507""";

}
