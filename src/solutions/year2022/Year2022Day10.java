package solutions.year2022;

import java.util.List;

import com.github.aoclib.api.InputParser;
import com.github.aoclib.solver.PuzzleSolver;

/**
 * <p>
 * AUTOGENERATED BY DayGenerator.java
 * </p>
 *
 * <p>
 * https://github.com/vilhok/aoc-lib
 * </p>
 *
 * <p>
 * Edits in this file will not be overwritten.
 * </p>
 *
 */
public class Year2022Day10 extends PuzzleSolver {

    class VM {
	int pc = 0;
	int x = 1;
	int cycles;
	Instruction[] program;
	Instruction inProgress;

	public VM(Instruction[] program) {
	    this.program = program;
	}

	public void cycle() {
	    cycles++;

	}

	void complete() {
//	    System.out.println(pc + " " + x + " " + (pc * x) + " " + cycles);
	    if (inProgress != null) {
		switch (inProgress.name) {
		case "addx":
		    x += inProgress.value;
		    inProgress = null;
		    break;
		}
		return;
	    }
	    Instruction c = program[pc++];
//	    System.out.println(c);
	    switch (c.name) {
	    case "noop":
		break;
	    case "addx":
		inProgress = c;
		break;
	    }
	}

	public long getSignal() {
	    return cycles * x;
	}

    }

    record Instruction(String name, int value) {

    }

    @Override
    public Object firstPart(InputParser input) {

	Instruction[] program = input.getLines().stream().map(x -> x.split(" "))
		.map(x -> new Instruction(x[0], x.length == 1 ? 0 : Integer.parseInt(x[1])))
		.toArray(Instruction[]::new);
	VM v = new VM(program);
	long result = 0;
	while (v.cycles != 19) {
	    v.cycle();
	    v.complete();
	}
	v.cycle();
	result = v.getSignal();
	v.complete();
	System.out.println("Result!" + result);
	while (v.pc != v.program.length) {
	    v.cycle();
//	    System.out.println(v.cycles);
	    if ((v.cycles - 20) % 40 == 0) {
		System.out.println("Value for " + (v.cycles));
		long cyc = v.getSignal();
		System.out.println(cyc + " " + v.cycles + " " + v.x);
		result += cyc;
	    }
	    v.complete();
	}
	return result;
    }

    @Override
    public Object secondPart(InputParser input) {
	Instruction[] program = input.getLines().stream().map(x -> x.split(" "))
		.map(x -> new Instruction(x[0], x.length == 1 ? 0 : Integer.parseInt(x[1])))
		.toArray(Instruction[]::new);
	VM v = new VM(program);
	int drawIdx = 0;
	while (v.pc != v.program.length) {
	    v.cycle();
	    if (Math.abs(drawIdx - v.x) <= 1) {
		System.out.print("#");
	    } else {
		System.out.print(".");
	    }

	    drawIdx++;
	    if (drawIdx >= 40) {
		drawIdx = 0;
		System.out.println();
	    }

	    v.complete();
	}
	return "EHPZPJGL";
    }

    /*
     * Optional: add tests for each part in the following methods
     *
     * These methods have blank implementations in superclass as well and can be
     * deleted if you don't want to include tests.
     *
     * Add test as follows:
     *
     * new Test("sampleinput", expectedSolution);
     *
     * Collect the tests from the task web page.
     */

    @Override
    protected void insertTestsPart1(List<Test> tests) {
	tests.add(new Test(large, 13140));
    }

    @Override
    protected void insertTestsPart2(List<Test> tests) {
//	tests.add(new Test(large, 13140));
    }

    String large = """
    	addx 15
    	addx -11
    	addx 6
    	addx -3
    	addx 5
    	addx -1
    	addx -8
    	addx 13
    	addx 4
    	noop
    	addx -1
    	addx 5
    	addx -1
    	addx 5
    	addx -1
    	addx 5
    	addx -1
    	addx 5
    	addx -1
    	addx -35
    	addx 1
    	addx 24
    	addx -19
    	addx 1
    	addx 16
    	addx -11
    	noop
    	noop
    	addx 21
    	addx -15
    	noop
    	noop
    	addx -3
    	addx 9
    	addx 1
    	addx -3
    	addx 8
    	addx 1
    	addx 5
    	noop
    	noop
    	noop
    	noop
    	noop
    	addx -36
    	noop
    	addx 1
    	addx 7
    	noop
    	noop
    	noop
    	addx 2
    	addx 6
    	noop
    	noop
    	noop
    	noop
    	noop
    	addx 1
    	noop
    	noop
    	addx 7
    	addx 1
    	noop
    	addx -13
    	addx 13
    	addx 7
    	noop
    	addx 1
    	addx -33
    	noop
    	noop
    	noop
    	addx 2
    	noop
    	noop
    	noop
    	addx 8
    	noop
    	addx -1
    	addx 2
    	addx 1
    	noop
    	addx 17
    	addx -9
    	addx 1
    	addx 1
    	addx -3
    	addx 11
    	noop
    	noop
    	addx 1
    	noop
    	addx 1
    	noop
    	noop
    	addx -13
    	addx -19
    	addx 1
    	addx 3
    	addx 26
    	addx -30
    	addx 12
    	addx -1
    	addx 3
    	addx 1
    	noop
    	noop
    	noop
    	addx -9
    	addx 18
    	addx 1
    	addx 2
    	noop
    	noop
    	addx 9
    	noop
    	noop
    	noop
    	addx -1
    	addx 2
    	addx -37
    	addx 1
    	addx 3
    	noop
    	addx 15
    	addx -21
    	addx 22
    	addx -6
    	addx 1
    	noop
    	addx 2
    	addx 1
    	noop
    	addx -10
    	noop
    	noop
    	addx 20
    	addx 1
    	addx 2
    	addx 2
    	addx -6
    	addx -11
    	noop
    	noop
    	noop""";

}
