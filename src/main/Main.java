package main;

import java.io.IOException;

import com.github.aoclib.api.AOCApi;
import com.github.aoclib.db.DBManager;
import com.github.aoclib.solver.AoC;

public class Main {

    public static void main(String[] args) throws IOException {

	DBManager.setFile("aoc.db");
	AOCApi.saveReponses = true;
	AoC s = new AoC(args);
	s.run();
    }
}
